.. aimms:module:: axll



Workbook Management
---------------------------

.. toctree::

   
.. aimms:externalprocedure:: OpenWorkBook(WorkbookFilename)



    This function loads an excel file so it can be manipulated with the functions of this library.
    It will make it the active workbook, and it's first sheet the active sheet.
    
    .. note::
    
        An error is issued when the workbook is already opened.
    
    When done with the workbook, you must call :any:`CloseWorkBook` to save and close.

    .. aimms:stringparameter:: WorkbookFilename
    
        :attribute Property: Input
    
    
    
        The path to an existing .xlsx or .xls file
    
.. aimms:externalprocedure:: CreateNewWorkBook(WorkbookFilename,FirstSheetName)



    This function creates a new excel file and opens it such that it can be manipulated with the functions of this library.
    If a file with the given name already exists, this file will be overwritten.
    
    When all modifications are made, you must call :any:`CloseWorkBook()` to save and close.

    .. aimms:stringparameter:: WorkbookFilename
    
        :attribute Property: Input
    
    
    
        The path to the .xlsx or .xls file that you want to create.
    
    .. aimms:stringparameter:: FirstSheetName
    
        :attribute Property: Optional
    
    
        (Optional) The name of the single sheet in the newly created workbook.
        If you leave this empty the sheet will be named "Sheet1".
    
.. aimms:externalprocedure:: CloseWorkBook(WorkbookFilename)



    This function closes the internal in-memory representation of the workbook that corresponds
    to the given file name.
    If any modifications have been made to this workbook, these will be saved back to the given file name.
    
    After this call, there is no active workbook and thus no active sheet.
    
    .. note::
    
        An error is issued when the workbook is not open.

    .. aimms:stringparameter:: WorkbookFilename
    
        :attribute Property: Input
    
    
    
        The name of an .xlsx or .xls file that was previously opened via a call to :any:`OpenWorkBook` or :any:`CreateNewWorkBook`.
    
.. aimms:externalprocedure:: SelectSheet(SheetName)



    This function will make the given sheet the active sheet.
    Most of the other functions in this library operate on the active sheet.

    .. aimms:stringparameter:: SheetName
    
        :attribute Property: Input
    
    
    
        The name of an existing sheet in the active workbook.
    
.. aimms:externalprocedure:: IsExistingSheet(SheetName)


    :attribute ReturnType: integer


    With this function you can check whether a sheet with the given name exists in the workbook.
    The function returns 1 if the sheet exists, 0 otherwise.

    .. aimms:stringparameter:: SheetName
    
        :attribute Property: Input
    
    
    
        The name of an existing sheet in the active workbook.
    
.. aimms:externalprocedure:: DeleteSheet(SheetName)



    This function will delete the specified sheet in the current workbook.
    If it is the currently selected sheet, you must select another sheet after this call before using any of 
    the functions that operate on the currently active sheet.

    .. aimms:stringparameter:: SheetName
    
        :attribute Property: Input
    
    
    
        The name of an existing sheet in the active workbook.
    
.. aimms:externalprocedure:: CreateSheet(SheetName,InsertBeforeThisSheet)




    This function will create a new sheet in the current workbook.

    .. aimms:stringparameter:: SheetName
    
        :attribute Property: Input
    
    
    
        The name of the new to be created sheet. If the sheet already exists an error is triggered.
    
    .. aimms:stringparameter:: InsertBeforeThisSheet
    
        :attribute Property: Optional
    
    
        (Optional) The new sheet will be inserted just to the left of this existing sheet. 
        If you leave this empty, the new sheet will be appended as last sheet.
    
.. aimms:externalprocedure:: CopySheet(SourceSheetName,NewSheetName,InsertBeforeThisSheet)



    This function will create a new sheet in the current workbook that is a 
    copy of an existing sheet.

    .. aimms:stringparameter:: InsertBeforeThisSheet
    
        :attribute Property: Optional
    
    
        (Optional) The new sheet will be inserted just to the left of this existing sheet. 
        If you leave this empty, the new sheet will be appended as last sheet.
    
    .. aimms:stringparameter:: SourceSheetName
    
        :attribute Property: Input
    
    
        The name of an existing sheet in the active workbook.
        The contents of this sheet will be copied to the newly created sheet.
    
    .. aimms:stringparameter:: NewSheetName
    
        :attribute Property: Input
    
    
    
        The name of the new to be created sheet. If the sheet already exists an error is triggered.
    
.. aimms:externalprocedure:: SelectWorkBook(WorkbookFilename)



    This function makes a previously loaded excel file the active workbook.
    It also makes it's last used sheet the active sheet.

    .. aimms:stringparameter:: WorkbookFilename
    
        :attribute Property: Input
    
    
    
        The name of an .xlsx or .xls file that was previously opened via a call to :any:`OpenWorkBook` or :any:`CreateNewWorkBook`.
    
.. aimms:externalprocedure:: CloseAllWorkBooks



    This function closes all workbooks that have been opened by calls to :any:`OpenWorkBook` or :any:`CreateNewWorkBook`.
    Calling this function is the same as calling :any:`CloseWorkBook` explicitly for every open workbook.

.. aimms:externalprocedure:: WorkBookIsOpen(WorkbookFilename)


    :attribute ReturnType: integer


    This function checks whether the given .xlsx or .xls file has previously been opened (and not yet closed) via
    a call to :any:`OpenWorkBook` or :any:`CreateNewWorkBook`.
    The function returns 1 if the workbook is open, or 0 otherwise.

    .. aimms:stringparameter:: WorkbookFilename
    
        :attribute Property: Input
    
    
    
        The path name of an .xlsx or .xls file.
    